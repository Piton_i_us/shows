﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Shows.JsonModels
{

    // Class JsonImage for JsonShows and JsonEpisodes classes

    #region  JsonImage class 
    public partial class JsonImage
    {
        [JsonProperty("medium")]
        public string Medium { get; set; }

        [JsonProperty("original")]
        public string Original { get; set; }
    }

    #endregion


    public static class Serialize
    {
        public static string ToJson(this JsonEpisodes[] self) => JsonConvert.SerializeObject(self, Converter.Settings);
        public static string ToJson(this JsonShows self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }


    public class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }   
}
