﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//    using Shows.Models;
//    var data = JsonEpisodes.FromJson(jsonString);

using System;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Shows.JsonModels
{


    public partial class JsonEpisodes
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("season")]
        public long Season { get; set; }

        [JsonProperty("number")]
        public long Number { get; set; }

        [JsonProperty("airdate")]
        public System.DateTime Airdate { get; set; }

        [JsonProperty("runtime")]
        public long Runtime { get; set; }

        [JsonProperty("image")]
        public JsonImage Image { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }


        public JsonEpisodes()
        {
            Id = 0;

            Url = "";

            Name = "";
            Season = 0;

            Number = 0;

            Airdate = System.DateTime.Now;

            Runtime = 0;

            Image = null;

            Summary = "";

        }


    }
    public partial class JsonEpisodes
    {
        public static JsonEpisodes[] FromJson(string json) => JsonConvert.DeserializeObject<JsonEpisodes[]>(json, Converter.Settings);
    }

  

   
}

