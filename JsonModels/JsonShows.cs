﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Shows.Models;
//
//    var data = JsonShows.FromJson(jsonString);

namespace Shows.JsonModels
{
    using System;
    using System.Net;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using J = Newtonsoft.Json.JsonPropertyAttribute;

    public partial class JsonShows
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string PurpleType { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("genres")]
        public List<string> Genres { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("runtime")]
        public long Runtime { get; set; }

        [JsonProperty("premiered")]
        public System.DateTime Premiered { get; set; }

        [JsonProperty("officialSite")]
        public object OfficialSite { get; set; }

        [JsonProperty("schedule")]
        public Schedule Schedule { get; set; }

        [JsonProperty("rating")]
        public Rating Rating { get; set; }

        [JsonProperty("weight")]
        public long Weight { get; set; }

        [JsonProperty("network")]
        public Network Network { get; set; }

        [JsonProperty("webChannel")]
        public object WebChannel { get; set; }

        [JsonProperty("externals")]
        public Externals Externals { get; set; }

        [JsonProperty("image")]
        public JsonImage Image { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("updated")]
        public long Updated { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }

    public partial class Externals
    {
        [JsonProperty("tvrage")]
        public object Tvrage { get; set; }

        [JsonProperty("thetvdb")]
        public long Thetvdb { get; set; }

        [JsonProperty("imdb")]
        public string Imdb { get; set; }
    }

  

    public partial class Links
    {
        [JsonProperty("self")]
        public Previousepisode Self { get; set; }

        [JsonProperty("previousepisode")]
        public Previousepisode Previousepisode { get; set; }
    }

    public partial class Previousepisode
    {
        [JsonProperty("href")]
        public string Href { get; set; }
    }

    public partial class Network
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("country")]
        public Country Country { get; set; }
    }

    public partial class Country
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }
    }

    public partial class Rating
    {
        [JsonProperty("average")]
        public long Average { get; set; }
    }

    public partial class Schedule
    {
        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("days")]
        public List<string> Days { get; set; }
    }

    public partial class JsonShows
    {
        public static JsonShows FromJson(string json) => JsonConvert.DeserializeObject<JsonShows>(json, Converter.Settings);
    }
    

}