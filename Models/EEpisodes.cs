﻿using System;
using System.Collections.Generic;

namespace Shows.Models
{
    public partial class EEpisodes
    {
        public int EpisodesId { get; set; }
        public string EpisodesUrl { get; set; }
        public string EpisodesName { get; set; }
        public short? EpisodesSeason { get; set; }
        public int? EpisodesNumber { get; set; }
        public DateTime? EpisodesAirdate { get; set; }
        public int? EpisodesImage { get; set; }
        public string EpisodesSummary { get; set; }
    }
}
