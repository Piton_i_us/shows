﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Shows.Models
{
    public partial class ShowsDbContext : DbContext
    {
        public virtual DbSet<EEpisodes> EEpisodes { get; set; }
        public virtual DbSet<TCountry> TCountry { get; set; }
        public virtual DbSet<TGenres> TGenres { get; set; }
        public virtual DbSet<TGenresDictionary> TGenresDictionary { get; set; }
        public virtual DbSet<TImage> TImage { get; set; }
        public virtual DbSet<TLanguage> TLanguage { get; set; }
        public virtual DbSet<TNetwork> TNetwork { get; set; }
        public virtual DbSet<TShows> TShows { get; set; }


        //DI in Startup.cs for DB Connecting 
        public ShowsDbContext(DbContextOptions<ShowsDbContext> options)
    : base(options)
        { }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EEpisodes>(entity =>
            {
                entity.HasKey(e => e.EpisodesId);

                entity.ToTable("e_episodes");

                entity.Property(e => e.EpisodesId)
                    .HasColumnName("episodes_Id")
                    .ValueGeneratedNever();

                entity.Property(e => e.EpisodesAirdate)
                    .HasColumnName("episodes_Airdate")
                    .HasColumnType("date");

                entity.Property(e => e.EpisodesImage).HasColumnName("episodes_Image");

                entity.Property(e => e.EpisodesName)
                    .HasColumnName("episodes_Name")
                    .HasMaxLength(30);

                entity.Property(e => e.EpisodesNumber).HasColumnName("episodes_Number");

                entity.Property(e => e.EpisodesSeason).HasColumnName("episodes_Season");

                entity.Property(e => e.EpisodesSummary)
                    .HasColumnName("episodes_Summary")
                    .HasColumnType("text");

                entity.Property(e => e.EpisodesUrl)
                    .HasColumnName("episodes_Url")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<TCountry>(entity =>
            {
                entity.HasKey(e => e.CountryId);

                entity.ToTable("t_Country");

                entity.Property(e => e.CountryId).HasColumnName("country_Id");

                entity.Property(e => e.CountryCode)
                    .HasColumnName("country_code")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CountryName)
                    .HasColumnName("country_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Timezone)
                    .HasColumnName("timezone")
                    .HasMaxLength(70)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TGenres>(entity =>
            {
                entity.HasKey(e => e.GenresId);

                entity.ToTable("t_Genres");

                entity.Property(e => e.GenresId)
                    .HasColumnName("genres_Id")
                    .ValueGeneratedNever();

                entity.Property(e => e.GenresName)
                    .HasColumnName("genres_Name")
                    .HasMaxLength(20);

                entity.HasOne(d => d.Genres)
                    .WithOne(p => p.InverseGenres)
                    .HasForeignKey<TGenres>(d => d.GenresId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_t_Genres_t_Genres");
            });

            modelBuilder.Entity<TGenresDictionary>(entity =>
            {
                entity.HasKey(e => e.GenreDictionaryId);

                entity.ToTable("t_Genres_Dictionary");

                entity.Property(e => e.GenreDictionaryId)
                    .HasColumnName("genre_dictionary_Id")
                    .ValueGeneratedNever();

                entity.Property(e => e.GenresId).HasColumnName("genres_Id");

                entity.Property(e => e.ShowId).HasColumnName("show_Id");

                entity.HasOne(d => d.Genres)
                    .WithMany(p => p.TGenresDictionary)
                    .HasForeignKey(d => d.GenresId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_t_Genres_Dictionary_t_Genres");
            });

            modelBuilder.Entity<TImage>(entity =>
            {
                entity.HasKey(e => e.ImageId);

                entity.ToTable("t_Image");

                entity.Property(e => e.ImageId).HasColumnName("image_Id");

                entity.Property(e => e.ImageMedium)
                    .HasColumnName("image_Medium")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ImageOriginal)
                    .HasColumnName("image_Original")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TLanguage>(entity =>
            {
                entity.HasKey(e => e.LanguageId);

                entity.ToTable("t_Language");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("language_Id")
                    .ValueGeneratedNever();

                entity.Property(e => e.LanguageName)
                    .HasColumnName("language_Name")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TNetwork>(entity =>
            {
                entity.HasKey(e => e.NetworkId);

                entity.ToTable("t_Network");

                entity.Property(e => e.NetworkId)
                    .HasColumnName("network_Id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.NetworkCountry).HasColumnName("network_country");

                entity.Property(e => e.NetworkName)
                    .HasColumnName("network_Name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Network)
                    .WithOne(p => p.TNetwork)
                    .HasForeignKey<TNetwork>(d => d.NetworkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_t_Network_t_Country");
            });

            modelBuilder.Entity<TShows>(entity =>
            {
                entity.HasKey(e => e.ShowId);

                entity.ToTable("t_Shows");

                entity.Property(e => e.ShowId)
                    .HasColumnName("show_Id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ShowGenres).HasColumnName("show_Genres");

                entity.Property(e => e.ShowImage).HasColumnName("show_Image");

                entity.Property(e => e.ShowLanguage).HasColumnName("show_Language");

                entity.Property(e => e.ShowName)
                    .IsRequired()
                    .HasColumnName("show_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.ShowNetwork).HasColumnName("show_Network");

                entity.Property(e => e.ShowOfficialSite)
                    .HasColumnName("show_OfficialSite")
                    .HasMaxLength(250);

                entity.Property(e => e.ShowPremiered)
                    .HasColumnName("show_Premiered")
                    .HasColumnType("date");

                entity.Property(e => e.ShowRating).HasColumnName("show_Rating");

                entity.Property(e => e.ShowSummary)
                    .HasColumnName("show_Summary")
                    .HasColumnType("text");

                entity.HasOne(d => d.ShowGenresNavigation)
                    .WithMany(p => p.TShows)
                    .HasForeignKey(d => d.ShowGenres)
                    .HasConstraintName("FK_t_Shows_t_Genres_Dictionary");

                entity.HasOne(d => d.ShowImageNavigation)
                    .WithMany(p => p.TShows)
                    .HasForeignKey(d => d.ShowImage)
                    .HasConstraintName("FK_t_Shows_t_Image");

                entity.HasOne(d => d.ShowLanguageNavigation)
                    .WithMany(p => p.TShows)
                    .HasForeignKey(d => d.ShowLanguage)
                    .HasConstraintName("FK_t_Shows_t_Language");

                entity.HasOne(d => d.ShowNetworkNavigation)
                    .WithMany(p => p.TShows)
                    .HasForeignKey(d => d.ShowNetwork)
                    .HasConstraintName("FK_t_Shows_t_Network");
            });
        }
    }
}
