﻿using System;
using System.Collections.Generic;

namespace Shows.Models
{
    public partial class TGenres
    {
        public TGenres()
        {
            TGenresDictionary = new HashSet<TGenresDictionary>();
        }

        public short GenresId { get; set; }
        public string GenresName { get; set; }

        public TGenres Genres { get; set; }
        public TGenres InverseGenres { get; set; }
        public ICollection<TGenresDictionary> TGenresDictionary { get; set; }
    }
}
