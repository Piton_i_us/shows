﻿using System;
using System.Collections.Generic;

namespace Shows.Models
{
    public partial class TGenresDictionary
    {
        public TGenresDictionary()
        {
            TShows = new HashSet<TShows>();
        }

        public int GenreDictionaryId { get; set; }
        public int ShowId { get; set; }
        public short GenresId { get; set; }

        public TGenres Genres { get; set; }
        public ICollection<TShows> TShows { get; set; }
    }
}
