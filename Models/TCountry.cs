﻿using System;
using System.Collections.Generic;

namespace Shows.Models
{
    public partial class TCountry
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string Timezone { get; set; }

        public TNetwork TNetwork { get; set; }
    }
}
