﻿using System;
using System.Collections.Generic;

namespace Shows.Models
{
    public partial class TLanguage
    {
        public TLanguage()
        {
            TShows = new HashSet<TShows>();
        }

        public short LanguageId { get; set; }
        public string LanguageName { get; set; }

        public ICollection<TShows> TShows { get; set; }
    }
}
