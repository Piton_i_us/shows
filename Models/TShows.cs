﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Shows.Models
{
    public partial class TShows
    {
        public int ShowId { get; set; }
       
        public string ShowName { get; set; }
        public short? ShowLanguage { get; set; }
        public int? ShowGenres { get; set; }
        public DateTime? ShowPremiered { get; set; }
        public string ShowOfficialSite { get; set; }
        public double? ShowRating { get; set; }
        public int? ShowNetwork { get; set; }
        public int? ShowImage { get; set; }
        public string ShowSummary { get; set; }

        public TGenresDictionary ShowGenresNavigation { get; set; }
        public TImage ShowImageNavigation { get; set; }
        public TLanguage ShowLanguageNavigation { get; set; }
        public TNetwork ShowNetworkNavigation { get; set; }
    }
}
