﻿using System;
using System.Collections.Generic;

namespace Shows.Models
{
    public partial class TImage
    {
        public TImage()
        {
            TShows = new HashSet<TShows>();
        }

        public int ImageId { get; set; }
        public string ImageMedium { get; set; }
        public string ImageOriginal { get; set; }

        public ICollection<TShows> TShows { get; set; }
    }
}
