﻿using System;
using System.Collections.Generic;

namespace Shows.Models
{
    public partial class TNetwork
    {
        public TNetwork()
        {
            TShows = new HashSet<TShows>();
        }

        public int NetworkId { get; set; }
        public string NetworkName { get; set; }
        public int? NetworkCountry { get; set; }

        public TCountry Network { get; set; }
        public ICollection<TShows> TShows { get; set; }
    }
}
