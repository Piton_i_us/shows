﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Shows.Models;

namespace Shows.Controllers
{
     

    [Route("api/[controller]")]
    public class ValuesController : Controller
    {

        #region  Controller attributes

        private readonly ShowsDbContext _ShowsRepository;
        private readonly ILogger<ValuesController> _logger;

        #endregion




        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
           
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            string returnResult = null;
            try
            {


                _logger.LogInformation("DB Method Called !!!");

                var genresResult =  _ShowsRepository.TGenres.Select(c => c.Genres).ToAsyncEnumerable();

                var jResult = JsonConvert.DeserializeObject(genresResult.ToString());
                //foreach (var item in genresResult)
                //{
                //    returnResult += item.GenresName;

                //    _logger.LogDebug("Get from DB - "+ item.GenresName);
                //}
                return returnResult;
            }
            catch (Exception mess)
            {
                _logger.LogInformation(mess, "DB Method Error", returnResult);
               
                return "NULL";
            }
             
           


        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


        #region Constructor 

        public ValuesController(ShowsDbContext showsRepository, ILoggerFactory loggerFactory)
        { 
            _logger = loggerFactory.CreateLogger<ValuesController>();
            _logger.LogInformation("ValuesController controller");
        
            _ShowsRepository = showsRepository;
           
        }
        #endregion

    }
}
